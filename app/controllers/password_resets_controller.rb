class PasswordResetsController < ApplicationController
  before_action :get_user,   only: [:edit, :update]
  before_action :validate_user, only: [:edit, :update]
  before_action :not_expired, only: [:edit, :update]

  def new
  end

  def edit
  end

  def update # This method is called by submit on password_reset/edit
    if(params[:user][:password].empty?)
      @user.errors.add(:password, "can't be empty")
      render 'edit'
    elsif (@user.update_attributes(user_params))
      login @user
      @user.update_attribute(:reset_digest, nil)
      flash[:success] = 'Password reset successfully.'
      redirect_to @user
    else
      render 'edit'
    end
  end

  def create # This method is called by submit on password_reset/new
    @user = User.find_by(email: params[:password_reset][:email].downcase)
    if !@user.nil?
      #create token, store to user, send email
      @user.create_reset_digest
      @user.send_password_reset
      flash[:success] = "Check your email for a password reset email"
      redirect_to login_path
    else
      flash.now[:danger] = 'Email address not found'
      render 'new'
    end
  end

  private

    def get_user
      @user = User.find_by(email: params[:email].downcase)
    end

    def validate_user
      unless(@user && @user.activated && @user.authenticated?(:reset, params["id"]))
        redirect_to login_url
      end
    end

    def not_expired
      if @user.password_reset_expired?
        flash[:danger] = "Password reset link was expired."
        redirect_to new_password_reset_url
      end
    end

    def user_params
      params.require(:user).permit(:password, :password_confirmation)
    end

end
