class SessionsController < ApplicationController
  def new
  end

  def create
  	user = User.find_by_email(params[:session][:email].downcase)
  	if user && user.authenticate(params[:session][:password])
      if user.activated
        flash[:success] = "Successfully logged in"
        login user
        redirect_to user
      else
        message  = "Account not activated. "
        message += "Check your email for the activation link."
        flash[:warning] = message
        redirect_to login_url
      end
  	else
  		flash.now[:danger] = "Email/Password combination was incorrect"
  		render 'new'
  	end
  end

  def delete
    log_out if logged_in
    redirect_to login_path
  end

end
