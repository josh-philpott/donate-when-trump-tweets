class StaticPagesController < ApplicationController
  def about
  end

  def home
  end

  def faq
  end

  def contact
  end
end
