class UsersController < ApplicationController
  def new
  	@user = User.new
  end

  def show
  	@user = User.find(params[:id])
  end

  def create
  	@user = User.new(param_user)
  	if @user.save
      @user.send_activation_email
  		flash[:info] = "Please check your email to activate your account."
  		redirect_to login_url
  	else
  		render 'new'
  	end
  end

  private

 	def param_user
 		params.require(:user).permit(:name, :email, :password, :password_confirmation)
 	end
end
