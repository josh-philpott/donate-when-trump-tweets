class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@donatewhentrumptweets.com'
  layout 'mailer'
end
