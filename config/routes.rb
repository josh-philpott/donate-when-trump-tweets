Rails.application.routes.draw do
  get 'password_resets/new'

  get 'password_resets/edit'

  get 'sessions/new'

  get 'users/new'

  root 'static_pages#home'
  get '/about', 	to:'static_pages#about'
  get '/faq', 		to:'static_pages#faq'
  get '/contact', 	to:'static_pages#contact'
  get '/signup', 	to:'users#new'
  post '/signup', 	to:'users#create'
  get '/login',		to:'sessions#new'
  post '/login',	to:'sessions#create'
  delete '/logout', to:'sessions#delete'
  resources :users
  resources :account_activations, only: [:edit]
  resources :password_resets, only: [:edit, :new, :create, :update]

end
