class FixActivatedAtColumnName < ActiveRecord::Migration[5.0]
  def change
  	rename_column :users, :activation_at, :activated_at
  end
end
