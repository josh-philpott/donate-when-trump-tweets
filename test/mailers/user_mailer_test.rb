require 'test_helper'

class UserMailerTest < ActionMailer::TestCase
  test "account_activation" do
    user = users(:bill)
    user.activation_token = User.new_token
    mail = UserMailer.account_activation(user)
    assert_equal "Activate Account - Donate When Trump Tweets", mail.subject
    assert_equal [user.email], mail.to
    assert_equal ["noreply@donatewhentrumptweets.com"], mail.from
    assert_match "Hey", mail.body.encoded
    assert_match user.name, mail.body.encoded
    assert_match user.activation_token, mail.body.encoded
    assert_match CGI.escape(user.email), mail.body.encoded
  end

  test "password_reset" do
    user = users(:bill)
    user.password_reset_token = User.new_token
    mail = UserMailer.password_reset(user)
    assert_equal "Reset Password - Donate When Trump Tweets", mail.subject
    assert_equal [user.email], mail.to
    assert_equal ["noreply@donatewhentrumptweets.com"], mail.from
    assert_match "Hey", mail.body.encoded
    assert_match user.name, mail.body.encoded
    assert_match user.password_reset_token, mail.body.encoded
    assert_match CGI.escape(user.email), mail.body.encoded
  end

end
