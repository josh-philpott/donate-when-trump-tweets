require 'test_helper'

class UserTest < ActiveSupport::TestCase
	def setup
		@user = User.new(name: "Joe Schmoe", email: "Joe@Schmoe.com", 
			password: "foobarz", password_confirmation: "foobarz")
	end

	test "user is valid" do
		assert @user.valid?
	end

	test "name should be present" do
		@user.name = "     "
		assert_not @user.valid?
	end

	test "name should not be to long" do
		@user.name = "a" * 51
		assert_not @user.valid?
	end

	test "email should be present" do
		@user.name = "     "
		assert_not @user.valid?
	end

	test "email should not be too long" do
		@user.email = "a" * 256 + "@gmail.com"
		assert_not @user.valid?
	end

	test "email must be email format" do
		@user.email = "abcdefg"
		assert_not @user.valid?
	end

	test "should accept valid emails" do
		valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
        valid_addresses.each do |valid_email|
        	@user.email = valid_email
        	assert @user.valid?, "#{valid_email.inspect} should be valid"
        end
	end

	test "should not accept invalid emails" do
		invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
        invalid_addresses.each do |invalid_email|
        	@user.email = invalid_email
        	assert_not @user.valid?, "#{invalid_email} should not be valid"
        end
    end

    test "email should be unique and case insensitive" do
    	duplicate_user = @user.dup
    	duplicate_user.email = @user.email.upcase
    	@user.save
    	assert_not duplicate_user.valid?
    end

    test "email addresses should be saved as lower-case" do
	    mixed_case_email = "Foo@ExAMPle.CoM"
	    @user.email = mixed_case_email
	    @user.save
	    assert_equal mixed_case_email.downcase, @user.reload.email
  	end

  	test "password should not be blank" do
  		@user.password = @user.password_confirmation = " " * 7
  		assert_not @user.valid?
  	end

  	test "password should be more than 5 characters" do
  		@user.password = @user.password_confirmation = "a" * 5
  		assert_not @user.valid?
		end

		#password reset
		test "password reset should fail if older than 2 hours" do
			@user.reset_sent_at = 3.hours.ago
			assert @user.password_reset_expired?
		end

		test "password reset should succeed when newer than 2 hours" do
			@user.reset_sent_at = 1.hours.ago
			assert_not @user.password_reset_expired?
		end

end
